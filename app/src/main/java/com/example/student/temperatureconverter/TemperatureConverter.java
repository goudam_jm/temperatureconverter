package com.example.student.temperatureconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;

public class TemperatureConverter extends AppCompatActivity{

    private Button toCelsius;
    private Button toFahrenheit ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_converter);

        toCelsius = (Button) findViewById(R.id.buttonToCelsius);
        toFahrenheit = (Button) findViewById(R.id.buttonToFahrenheit);

        if(findViewById(R.id.fragment) != null){

            if(savedInstanceState != null){
                return;
            }

            TemperatureConverterFragment temperatureConverterFragment = new TemperatureConverterFragment();

            temperatureConverterFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction().add(R.id.fragment, temperatureConverterFragment).commit();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_temperature_converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {

            AboutFragment aboutFragment = new AboutFragment();

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment, aboutFragment);
            fragmentTransaction.addToBackStack(null);

            fragmentTransaction.commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
